﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Library.Helpers
{
    public static class ExtensionHelper
    {
        public static MvcHtmlString ListItems(this HtmlHelper htmlHelper, IEnumerable<(string, int)> items)  
        {  
            var ulElement=new TagBuilder("ul");  
            foreach (var item in items)  
            {  
                var liElement=new TagBuilder("li");  
                liElement.SetInnerText($"{item.Item1} - {item.Item2}");  
                ulElement.InnerHtml += liElement.ToString();  
            }  
            return new MvcHtmlString(ulElement.ToString());  
        }  
    }
}