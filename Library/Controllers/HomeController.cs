﻿using System.Web.Mvc;
using Library.App_Data;

namespace Library.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Articles = StaticDb.Articles;
            
            return View();
        }
    }
}