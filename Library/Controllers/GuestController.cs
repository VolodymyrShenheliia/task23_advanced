﻿using System;
using System.Linq;
using System.Web.Mvc;
using Library.App_Data;

namespace Library.Controllers
{
    public class GuestController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Feedbacks = StaticDb.Feedbacks;
            
            return View();
        }

        [HttpGet]
        public ActionResult PostData(string name, string feedback)
        {
            return PostData(name, feedback, DateTime.Now);
        }

        [HttpPost]
        public ActionResult PostData(string name, string feedback, DateTime date)
        {
            StaticDb.Feedbacks.Add((StaticDb.Feedbacks.Last().Item1+1, name, feedback, date));
            
            return RedirectToAction("Index", "Guest");
        }
        
    }
}