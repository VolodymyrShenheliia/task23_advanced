﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.App_Data;

namespace Library.Controllers
{
    public class QuestionnaireController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.RadioAnswers = StaticDb.RadioAnswers;
            ViewBag.CheckAnswers = StaticDb.CheckAnswers;
            return View();
        }

        [HttpGet]
        public ActionResult QuestionnaireResult()
        {
            Console.WriteLine(Request.QueryString["radioReading"].Split(',').First());
            return QuestionnaireResult(Request.QueryString["Name"],
                Request.QueryString["radioReading"]?.Split(',').First(),
                Request.QueryString["checkBooks"]?.Split(','));
        }

        [HttpPost]
        public ActionResult QuestionnaireResult(string name, string reading, string[] books)
        {
            ViewBag.Name = name;

            ViewBag.Message = reading != "Don't read" ? "You are reading. It's great!!!" : "You are not reading. It's awful!!!";

            var userReading = StaticDb.RadioAnswers.FirstOrDefault(x => x.Item1 == reading);
            var indexOfReading = StaticDb.RadioAnswers.IndexOf(userReading);
            
            userReading.Item2++;
            
            StaticDb.RadioAnswers[indexOfReading] = userReading;

            if (books != null)
            {
                foreach (var book in books)
                {
                    var condition = StaticDb.CheckAnswers.Count(x => x.Item1 == book) == 1;

                    if (condition)
                    {
                        var userBook = StaticDb.CheckAnswers.FirstOrDefault(x => x.Item1 == book);
                        var indexOfBook = StaticDb.CheckAnswers.IndexOf(userBook);

                        userBook.Item2++;

                        StaticDb.CheckAnswers[indexOfBook] = userBook;  
                    }
                }
            }
            
            ViewBag.CheckAnswers = StaticDb.CheckAnswers; 
            ViewBag.RadioAnswers = StaticDb.RadioAnswers;

            return View();
        }
        
    }
}