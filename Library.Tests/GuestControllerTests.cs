﻿using System.Web.Mvc;
using Library.App_Data;
using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library.UnitTests
{
    [TestClass]
    public class GuestControllerTests
    {
        [TestMethod]
        public void GuestController()
        {
            var controller = new GuestController();
            
            var feedbacksList = controller.Index() as ViewResult;
            
            Assert.AreEqual(StaticDb.Feedbacks, feedbacksList?.ViewData["Feedbacks"]);

        }
    }
}