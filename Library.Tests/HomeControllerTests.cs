﻿using System.Web.Mvc;
using Library.App_Data;
using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library.UnitTests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void HomeController()
        {
            var controller = new HomeController();
            
            var articlesList = controller.Index() as ViewResult;
            
            Assert.AreEqual(StaticDb.Articles, articlesList?.ViewData["Articles"]);
        }
    }
}
